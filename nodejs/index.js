// Upload single panorama files

const express = require('express');
const multer = require('multer');
const passport = require('passport');
const Strategy = require('passport-http-bearer').Strategy;
const upload = multer({dest: 'uploads/'});
const moment = require('moment');
require('moment-round');
const fs = require('fs');

const app = express();

const
    CORRECT_AUTH_TOKEN = 'iAmARLYSTroNGTkN!!111',
    // Take photo each 10 seconds
    SHOT_INTERVAL = 10;

nextShotTime = moment().ceil(10, 'seconds');
shotId = 0;
prefix='';

function generatePrefix() {
    require('crypto').randomBytes(8, function(err, buffer) {
        prefix = buffer.toString('hex');
    });
}

function updateNextShotTime() {
    let roundTime = moment().add(SHOT_INTERVAL, 'seconds');
    nextShotTime = roundTime.floor(10, 'seconds');
    shotId++;
}

generatePrefix();
updateNextShotTime();
// setInterval(updateNextShotTime, SHOT_INTERVAL*1000);

passport.use(new Strategy(
    function (token, done) {
        // We can do real auth here, but lets authenticate with predefined secret right now
        // TODO: we can change it later
        if (token === CORRECT_AUTH_TOKEN) {
            return done(null, {});
        } else {
            return done(new Error('Invalid magic token'));
        }
    }
));

// Endpoint /schedule returns JSON including
// * command = shot / pause
// * time = time to shot or next server query in IS8601 format
// * shotId = sequential shot id

app.get('/schedule',
    passport.authenticate('bearer', {session: false}),
    function (req, res, next) {

        now = moment();

        console.log('Scheduled');
        console.log(now.format("YYYY-MM-DDTHH:mm:ss.SSSZZ"));
        console.log(nextShotTime.format("YYYY-MM-DDTHH:mm:ss.SSSZZ"));
        console.log(now >= nextShotTime);
        if(now >= nextShotTime) {
            
            updateNextShotTime();
            console.log('Updated Next Time:' + nextShotTime.format("YYYY-MM-DDTHH:mm:ss.SSSZZ"));
            res.json({
                command: 'shot',
                time: nextShotTime.format("YYYY-MM-DDTHH:mm:ss.SSSZZ"),
                shotId: shotId,
                canShot: true
            });
        }else{
            res.json({
                command: 'shot',
                time: nextShotTime.format("YYYY-MM-DDTHH:mm:ss.SSSZZ"),
                shotId: shotId,
                canShot: false
            });
        }
    });

app.post('/shot',
    passport.authenticate('bearer', {session: false}),
    upload.single('shot'),
    function (req, res, next) {
        // req.file is the `shot` file
        // req.body will hold the text fields, if there were any
        const cam = +req.body.cam;
        const shotId = +req.body.shotId;
        const latitude = +req.body.latitude;
        const longitude = +req.body.longitude;
        fs.rename(req.file.path, 'uploads/shot-'+prefix+'-'+nextShotTime+'-'+shotId+'-'+cam+'.jpg');
        //console.log(req.file);
        //console.log(req.body);

        fs.writeFile('uploads/shot-'+prefix+'-'+nextShotTime+'-'+shotId+'-'+cam+'.txt', latitude + "/" + longitude, function(err) {
		    if(err) {
		        return console.log(err);
		    }
		    console.log("The file was saved!");
		});

        res.json({
            status: true
        });
    });

app.listen(3000, function () {
    console.log('Pano app listening on port 3000!');
});
