# Pano server

The app synchronizes the Pano apps and stores photos

### How to install

```
npm install
```

Then run 

```
node index.js
```

The application will trigger shots every 10 sec. You can find uploaded photos in "uploads" directory.

It listen on **3000** port

In the app, you should specify your server's ip (or hostname) and this port divided by colon

Example:
```
localhost:3000
```