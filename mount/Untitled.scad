/* iphone dimensions

iPhone 5s
123.8 mm (4.87 in) H
58.6 mm (2.31 in) W
7.6 mm (0.30 in) D

iPhone 4s
Height: 4.5 inches (115.2 mm)
Width: 2.31 inches (58.6 mm)
Depth: 0.37 inch (9.3 mm)

*/

margin = 2; // space around the phone so it fits
iPhoneHeight = 115.2;
iPhoneWidth = 58.6 + margin;
iPhoneDepth = 9.3 + margin;
numberOfPhones = 8;
wallThickness = 1;

push = (iPhoneWidth/2+wallThickness)/tan((360/numberOfPhones)/2);

echo(push);

union(){
for(i = [0:360/numberOfPhones:360]) {
    rotate(a=[0,0,i]) {
        translate([35-(iPhoneWidth+wallThickness*2)/2,-(56-push-wallThickness),0]) {
            difference() {
                cube([iPhoneWidth+wallThickness*2, iPhoneDepth+wallThickness*2, iPhoneHeight/3+wallThickness*2]);
                
                translate([wallThickness,wallThickness,wallThickness]) {
                    color("red"){
                        cube([iPhoneWidth, iPhoneDepth, iPhoneHeight]);
                    }
                }
            }
        }
    }
}
size = 33;//push+iPhoneDepth+wallThickness*2-20;
echo(size);
color("blue") {
cylinder(wallThickness-.1,size,size,center=false);
}
}

    
    
    