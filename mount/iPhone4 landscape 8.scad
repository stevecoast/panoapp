/* iphone dimensions

iPhone 5s
123.8 mm (4.87 in) H
58.6 mm (2.31 in) W
7.6 mm (0.30 in) D

iPhone 4s
Height: 4.5 inches (115.2 mm)
Width: 2.31 inches (58.6 mm)
Depth: 0.37 inch (9.3 mm)

*/

margin = 0.750; // space around the phone so it fits
iPhoneWidth = 115.2 + margin;
iPhoneHeight= 58.6 + margin;
iPhoneDepth = 9.3 + margin;
numberOfPhones = 5;
wallThickness = 1;



module container() {
difference() {
        cube([iPhoneWidth+wallThickness*2, iPhoneDepth+wallThickness*2, iPhoneHeight/3*2+wallThickness*2]);
          translate([wallThickness,wallThickness,wallThickness]) {
            cube([iPhoneWidth, iPhoneDepth, iPhoneHeight]);
            translate([wallThickness+20,wallThickness-10,wallThickness+5]) {
             // cube([iPhoneWidth-40, iPhoneDepth+50, iPhoneHeight]);
            }
          }
}
}


container();
wallThickness = 1.2; // allow for the plastic to not be super accurate

translate([50,13,0]){
rotate(a=360/numberOfPhones) {
    difference() {
        union(){
        cube([iPhoneWidth+wallThickness*4-100, iPhoneDepth+wallThickness*4, iPhoneHeight/3*2+wallThickness*2]);
        cylinder(wallThickness,12,12);
            }
            translate([wallThickness,wallThickness,0*wallThickness]) {
        cube([iPhoneWidth+wallThickness*2+100, iPhoneDepth+wallThickness*2, iPhoneHeight+wallThickness*2]);
          }
}
}
}
