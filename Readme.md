# Hot to use

## Run the server 

For the server in nodejs/ folder:

```
npm install
```

Then run 

```
node index.js
```

The application will trigger shots every 10 sec. You can find uploaded photos in "uploads" directory.

It listen on **3000** port

In the app, you should specify your server's ip (or hostname) and this port divided by colon

Example:
```
localhost:3000
```

## Run client apps

* Install Pano.app via iTunes on all devices
* Run the app
* Specify correct hostname:port of the server
* Specify unique camera number

Each 10 seconds apps will make a shot and send it to the server. 
You can find them in "uploads/" folder

