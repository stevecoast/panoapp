//
//  NetworkOp.m
//  Pano
//
//  Created by Alexey Petushkov on 24.01.17.
//  Copyright © 2017 Steve Coast. All rights reserved.
//

#import "NetworkOp.h"

@interface NetworkOp ()

@end

@implementation NetworkOp

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(nullable NSError *)error {
    
}

/* If an application has received an
 * -application:handleEventsForBackgroundURLSession:completionHandler:
 * message, the session delegate will receive this message to indicate
 * that all messages previously enqueued for this session have been
 * delivered.  At this time it is safe to invoke the previously stored
 * completion handler, or to begin any internal updates that will
 * result in invoking the completion handler.
 */
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session NS_AVAILABLE_IOS(7_0) {
    
}

- (void) getSchedule:(nonnull void (^)( NSError* __nullable error, NSData* __nullable result)) callback withHostname:(NSString* _Nonnull)hostname {
    // make an asynchronous request
    NSString *urlString = [NSString stringWithFormat:@"http://%@/schedule", hostname];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"Bearer iAmARLYSTroNGTkN!!111" forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request
                                                   completionHandler:^(NSData *data,
                                                                       NSURLResponse *response,
                                                                       NSError *error) {
                                                       NSString* responseStr  = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                       //if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                                       //NSLog(@"Http status %d", ((NSHTTPURLResponse*)response).statusCode);
                                                       //}
                                                       NSLog(@"CMD: %@", responseStr);
                                                       if (error) {
                                                           NSLog(@"Error: %@", [error localizedDescription]);
                                                           [self.delegate sendStatus:[error localizedDescription]];
                                                       }
                                                       (callback)(error, data);
                                                       
                                                   }];
    [getDataTask resume];
}

- (void) upload:(NSData* _Nonnull) data
         forCam:(NSString* _Nonnull)camNumber
      andShotId:(NSNumber* _Nonnull)shotId
   withHostname:(NSString* _Nonnull)hostname
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    double latitude = [userDefaults doubleForKey:ID_LAT];
    double longitude = [userDefaults doubleForKey:ID_LNG];
    
    // make an asynchronous request
    NSString *urlString = [NSString stringWithFormat:@"http://%@/shot", hostname];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSString *boundary = [self generateBoundaryString];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"Bearer iAmARLYSTroNGTkN!!111" forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    
    NSDictionary* params = @{
                             @"cam": camNumber,
                             @"shotId": shotId,
                             @"latitude": [NSNumber numberWithDouble:latitude],
                             @"longitude": [NSNumber numberWithDouble:longitude]
                             };
    NSString* fileName = [NSString stringWithFormat:@"shot-%@-%@.jpg", shotId, camNumber];
    NSData *httpBody = [self createBodyWithBoundary:boundary
                                         parameters:params
                                           fileName:fileName
                                               data:data
                                           mimeType:@"image/jpeg"
                                          fieldName:@"shot"];
    request.HTTPBody = httpBody;
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"error = %@", error);
            [self.delegate sendStatus:[NSString stringWithFormat: @"Upload Failed: %@", error]];
            return;
        }
        
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"result = %@", result);
        [self.delegate sendStatus:@"Upload Success"];
    }];
    [task resume];
}

- (NSString *)generateBoundaryString
{
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                          fileName:(NSString*)fileName
                              data:(NSData*)data
                          mimeType:(NSString*)mimetype
                         fieldName:(NSString *)fieldName
{
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, fileName] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:data];
    [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

@end