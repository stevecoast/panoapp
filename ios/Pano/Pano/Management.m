//
//  Management.m
//  Pano
//
//  Created by Alexey Petushkov on 25.01.17.
//  Copyright © 2017 Steve Coast. All rights reserved.
//

#import "Management.h"

@interface Management ()

@end

@implementation Management

NSTimer* nextTask = nil;
NetworkOp* networkOp = nil;
LLSimpleCamera* camera;

BOOL cameraGranted = NO;
CGFloat JPEG_QUALITY = 80.0f;

- (Management*) init {
    networkOp = [[NetworkOp alloc] init];
    networkOp.delegate = self;
    _commandCenterHostname = @"localhost";
    [self loadSettings];
//    [self getNextCommand];
    [LLSimpleCamera requestCameraPermission:^(BOOL granted) {
        camera = [[LLSimpleCamera alloc] initWithQuality:AVCaptureSessionPresetHigh
                                                position:LLCameraPositionRear
                                            videoEnabled:NO];
        cameraGranted = granted;
        [camera start];
    }];
    
    [self scheduleNewCommandRetrieval];

    return [super init];
}

NSString* ID_HOSTNAME = @"HOSTNAME";
NSString* ID_CAM = @"1";

- (void) saveLocation: (double) lat lng: (double) lng {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setDouble:lat forKey:ID_LAT];
    [userDefaults setDouble:lng forKey:ID_LNG];
    
    [userDefaults synchronize];
}

- (void) loadSettings {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* s = [userDefaults stringForKey:ID_HOSTNAME];
    if ( s != nil ) {
        self.commandCenterHostname = s;
    }
    s = [userDefaults stringForKey:ID_CAM];
    if ( s != nil ) {
        self.camNumber = s;
    }
}

- (void) persistSettings {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:self.commandCenterHostname forKey:ID_HOSTNAME];
    [userDefaults setValue:self.camNumber forKey:ID_CAM];
}

- (void) cancelNextCommand {
    if (nextTask) {
        [nextTask invalidate];
        nextTask = nil;
    }
}

- (NSDate*) parseIsoDate: (NSString*)input {
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    //correcting format to include seconds and decimal place
    dateFormat.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
    // Always use this locale when parsing fixed format date strings
    NSLocale* posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormat.locale = posix;
    NSDate* output = [dateFormat dateFromString:input];
    return output;
}

-(void) scheduleNewCommandRetrieval{
    NSLog(@"Scheduled retry");
    NSTimer* nextTask = [NSTimer timerWithTimeInterval:(1.0f)
                                     target:self
                                   selector:@selector(getNextCommand)
                                   userInfo:@{
                                              @"command": @"pause"
                                            }
                                    repeats: YES];
    [[NSRunLoop mainRunLoop] addTimer:nextTask forMode:NSDefaultRunLoopMode];
}

-(void) getNextCommand {
    [self cancelNextCommand];
    [networkOp getSchedule:^(NSError * _Nullable error, NSData * _Nullable result){
        if (result) {
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingMutableContainers error:nil];
            NSString *command = [json valueForKey:@"command"];
            NSNumber *shotId = [json valueForKey:@"shotId"];
            NSString *shotTime = [json valueForKey:@"time"];
            Boolean canShot = [[json valueForKey:@"canShot"] boolValue];
//            NSLog(@"Result = %@",json);
            NSLog(@"%@ at %@ with %@", command, shotTime, shotId);
            NSDate* nextShotTime = [self parseIsoDate:shotTime];
            NSDate* currentDate = [[NSDate alloc] init];
            NSLog(@"Next date is %@ (now is %@)", nextShotTime, currentDate);
//            if ([currentDate compare:nextShotTime] == NSOrderedAscending) {
            NSLog(@"CanShot=%@", canShot?@"true":@"false");
            if (canShot) {
//                NSTimer* nextTask1 = [NSTimer timerWithTimeInterval:(0.0f)
//                                                      target:self
//                                                    selector:@selector(timerFireMethod:)
//                                                    userInfo:@{
//                                                               @"command": command,
//                                                               @"shotId": shotId
//                                                               }
//                                                     repeats:NO];
//                [[NSRunLoop mainRunLoop] addTimer:nextTask1 forMode:NSDefaultRunLoopMode];
                
                [self canShotPhoto:command shotId:shotId];
            } else {
//                [self scheduleNewCommandRetrieval];
            }
        } else {
            NSLog(@"No result");
//            [self scheduleNewCommandRetrieval];
        }
    } withHostname:self.commandCenterHostname];
}

- (void)timerFireMethod:(NSTimer *)timer {
    NSDictionary* dict = timer.userInfo;
    NSString* command = dict[@"command"];
    [self canShotPhoto:command shotId:self.shotId];
}

- (void) canShotPhoto: (NSString*) command shotId:(NSNumber *)shotId {
    
    if ([command isEqualToString:@"shot"]) {
        self.shotId = shotId;
        NSLog(@"Making shot for %@", self.shotId);
        [self takePhoto];
        nextTask = nil;
//        [self getNextCommand];
    } else {
        if ([command isEqualToString:@"pause"]){
            nextTask = nil;
//            [self getNextCommand];
        }
    }
}

- (void) takePhoto {
    NSLog(@"Capturing photo");
    [camera capture:^(LLSimpleCamera *camera, UIImage *image, NSDictionary *metadata, NSError *error) {
        if(!error) {
            // we should stop the camera, since we don't need it anymore. We will open a new vc.
            // this very important, otherwise you may experience memory crashes
            [camera stop];
            NSLog(@"We have captured photo");
            [self.delegate sendStatus:@"We have captured photo"];
            [self sendPhoto:image];
            [camera start];
        } else {
            NSLog(@"Capture failed %@", [error localizedDescription]);
            [self.delegate sendStatus:[NSString stringWithFormat: @"Photo Capture failed: %@", [error localizedDescription]]];
        }
    }];
}

- (void) sendPhoto:(UIImage*) image {
    
    if (!self.camNumber) {
        [self.delegate sendStatus:@"Cam Number is not added"];
    }else{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSData* jpeg = UIImageJPEGRepresentation(image, JPEG_QUALITY);
            [networkOp upload:jpeg forCam:self.camNumber andShotId:self.shotId withHostname:self.commandCenterHostname];
        });
    }
}

- (void) sendStatus:(NSString *)status {
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.delegate sendStatus:status];
    });
}

@end
