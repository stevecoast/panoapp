//
//  NetworkOp.h
//  Pano
//
//  Created by Alexey Petushkov on 24.01.17.
//  Copyright © 2017 Steve Coast. All rights reserved.
//

#ifndef NetworkOp_h
#define NetworkOp_h

#define ID_LAT @"latitude"
#define ID_LNG @"langitude"

@import Foundation;

@protocol NetworkOpDelegate <NSObject>

-(void) sendStatus: (NSString*) status;

@end

@interface NetworkOp : NSObject<NSURLSessionDelegate>

@property (strong, nonatomic) id <NetworkOpDelegate> delegate;

/* The last message a session receives.  A session will only become
 * invalid because of a systemic error or when it has been
 * explicitly invalidated, in which case the error parameter will be nil.
 */
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(nullable NSError *)error;

/* If an application has received an
 * -application:handleEventsForBackgroundURLSession:completionHandler:
 * message, the session delegate will receive this message to indicate
 * that all messages previously enqueued for this session have been
 * delivered.  At this time it is safe to invoke the previously stored
 * completion handler, or to begin any internal updates that will
 * result in invoking the completion handler.
 */
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session NS_AVAILABLE_IOS(7_0);


- (void) getSchedule:(nonnull void (^)( NSError* __nullable error, NSData* __nullable result)) callback withHostname:(NSString* _Nonnull)hostname;

- (void) upload:(NSData* _Nonnull) data forCam:(NSString* _Nonnull)camNumber andShotId:(NSNumber* _Nonnull)shotId withHostname:(NSString* _Nonnull)hostname;

@end

#endif /* NetworkOp_h */
