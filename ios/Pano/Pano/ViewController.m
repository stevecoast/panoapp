//
//  ViewController.m
//  Pano
//
//  Created by Alexey Petushkov on 24.01.17.
//  Copyright © 2017 Steve Coast. All rights reserved.
//

#import "ViewController.h"
#import "INTULocationManager.h"

#define STATUS_CAPACITY 10
@interface ViewController () {
    
    Management* management;
    NSMutableArray *statusArray;
    INTULocationManager *locMgr;
    NSTimer *timer;
}

@end

@implementation ViewController



- (void)viewDidLoad {
    management = [[Management alloc] init];
    management.delegate = self;
    
    statusArray = [[NSMutableArray alloc] init];
    
    self.hostNameEdit.text = management.commandCenterHostname;
    self.camNumEdit.text = management.camNumber;

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.camNumEdit addTarget:self
                          action:@selector(onCamNumberChanged:)
                forControlEvents:UIControlEventEditingChanged];
    
    locMgr = [INTULocationManager sharedInstance];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:10.0f
                                     target:self selector:@selector(getGPSLocation) userInfo:nil repeats:YES];
    
    /*
    [locMgr subscribeToLocationUpdatesWithDesiredAccuracy:INTULocationAccuracyHouse
                                                    block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                        if (status == INTULocationStatusSuccess) {
                                                            // A new updated location is available in currentLocation, and achievedAccuracy indicates how accurate this particular location is.
                                                            
                                                            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                                            double latitude = [userDefaults doubleForKey:ID_LAT];
                                                            double longitude = [userDefaults doubleForKey:ID_LNG];
                                                            
                                                            if (latitude == currentLocation.coordinate.latitude && longitude == currentLocation.coordinate.longitude) {
                                                                
                                                            }else {
                                                                NSLog(@"Current Location=%@", currentLocation);
                                                            
                                                                [management saveLocation:currentLocation.coordinate.latitude lng:currentLocation.coordinate.longitude];
                                                            }
                                                        }
                                                        else {
                                                            // An error occurred, more info is available by looking at the specific status returned. The subscription has been kept alive.
                                                        }
                                                    }];
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    if (timer != nil) {
        [timer invalidate];
        timer = nil;
    }
}

- (IBAction)getScheduleClick:(id)sender {
    [management getNextCommand];
}

- (IBAction)onHostnameChanged:(id)sender {
    management.commandCenterHostname = self.hostNameEdit.text;
    [management persistSettings];
}

- (IBAction)onCamNumberChanged:(id)sender {
    
    management.camNumber = self.camNumEdit.text;
    [management persistSettings];
}

- (void) getGPSLocation {
    
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                 // currentLocation contains the device's current location.
                                                 
//                                                 NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//                                                 double latitude = [userDefaults doubleForKey:ID_LAT];
//                                                 double longitude = [userDefaults doubleForKey:ID_LNG];
                                                 
                                                 NSLog(@"Current Location=%@", currentLocation);
                                                 
                                                 [management saveLocation:currentLocation.coordinate.latitude lng:currentLocation.coordinate.longitude];
                                             }
                                             else if (status == INTULocationStatusTimedOut) {
                                                 // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                 // However, currentLocation contains the best location available (if any) as of right now,
                                                 // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                             }
                                             else {
                                                 // An error occurred, more info is available by looking at the specific status returned.
                                             }
                                         }];
}
- (void) sendStatus:(NSString *)status {
    
    [self addStatus:status];
    self.statusTextView.text = [self statusToString];
}

- (void) addStatus:(NSString *)status {
    
    [statusArray insertObject:status atIndex:0];
    if ([statusArray count] > STATUS_CAPACITY) {
        [statusArray removeLastObject];
    }
}

- (NSString *) statusToString {
    NSString *str = @"";
    
    NSInteger count = statusArray.count;
    if (statusArray.count > STATUS_CAPACITY) {
        count = STATUS_CAPACITY;
    }
    for (NSInteger index = count-1; index >=0; index--) {
        if (str.length == 0) {
            str = [statusArray objectAtIndex:index];
        }else {
            str = [NSString stringWithFormat:@"%@\n%@", str, [statusArray objectAtIndex:index]];
        }
    }
    
    return str;
}

@end
