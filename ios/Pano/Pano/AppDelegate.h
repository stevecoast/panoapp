//
//  AppDelegate.h
//  Pano
//
//  Created by Alexey Petushkov on 24.01.17.
//  Copyright © 2017 Steve Coast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

