//
//  ViewController.h
//  Pano
//
//  Created by Alexey Petushkov on 24.01.17.
//  Copyright © 2017 Steve Coast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkOp.h"
#import "Management.h"

@interface ViewController : UIViewController<ManagementDelegate>

@property (weak, nonatomic) IBOutlet UITextField *hostNameEdit;
@property (weak, nonatomic) IBOutlet UITextField *camNumEdit;

@property (weak, nonatomic) IBOutlet UITextView *statusTextView;

@property (weak, nonatomic) IBOutlet UITextField *onCamNumberUpdated;

@end

