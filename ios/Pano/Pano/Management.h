//
//  Management.h
//  Pano
//
//  Created by Alexey Petushkov on 25.01.17.
//  Copyright © 2017 Steve Coast. All rights reserved.
//

#ifndef Management_h
#define Management_h

@import Foundation;
#import <LLSimpleCamera/LLSimpleCamera.h>
#import "NetworkOp.h"

@protocol ManagementDelegate <NSObject>

-(void) sendStatus: (NSString*) status;

@end

@interface Management : NSObject<NetworkOpDelegate>

@property (strong, nonatomic) id <ManagementDelegate> delegate;

- (Management*) init;
- (void) getNextCommand;
- (void) takePhoto;
- (void) persistSettings;
- (void) saveLocation: (double) lat lng: (double) lng;

@property NSString* commandCenterHostname;
@property NSString* camNumber;
@property NSNumber* shotId;
@end

#endif /* Management_h */
